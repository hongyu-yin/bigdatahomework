#大数据抽样作业
#utf-8
library(dplyr)
library(sampling)
library(ggplot2)
setwd("C:\\Users\\YHY\\Desktop\\Temp\\sampling_homework")
#-----------------------------------------------
#简单随机抽样
#-----------------------------------------------
#读取数据
loandata = read.csv("LoanStats3c.csv",skip=1,stringsAsFactors = FALSE)
loandata_srs = na.omit(loandata[,"annual_inc"])
loandata_srs
#绘制直方图
data1 = cut(loandata_srs,breaks = c(0,10000*(1:10),150000,100000*(2:5),max(loandata_srs)))
barplot(summary(data1),xlab="收入区间",ylab="频数",main="年收入频数分布直方图",border=NA,col="grey45")
#确定样本量
length(loandata_srs)
x = seq(6,20,0.05)
y = 2^x
sampsize = round(y)[-c(238:281)]
sampsize
#共得到238组样本容量

#简单随机抽样
N_srs = length(loandata_srs)
#计算总体中每个水平出现的频率
PD_srs = table(data1)/N_srs
#定义简单随机抽样的函数
set.seed(20171101)
n = length(sampsize)
fun_srs <- function(i) {
  p <- sample(loandata_srs,i)
  p <- c(p,matrix(NA,1,sampsize[n]-length(p)))
}
sampsize  = as.matrix(sampsize)
#简单随机抽样
m_srs = data.frame(apply(sampsize,1,fun_srs))
#定义求样本质量的函数
J = NULL
fun_srsq <- function(datasam1) {
  datasam11 <- cut(na.omit(datasam1),breaks = c(0,10000*(1:10),150000,100000*(2:5),max(loandata_srs)))
  PS <- table(datasam11)/length(na.omit(datasam1))+0.0000000001
  J <- sum((PS-PD_srs)*(log(PS/PD_srs)))
  q <- exp(-J)
  return(q)
}
#求每个样本容量下的样本质量
Q1 <- apply(m_srs,2,fun_srsq)

#-----------------------------------------------
#分层随机抽样
#-----------------------------------------------
#以grade为分层依据
loandata_ss = na.omit(loandata[,c("grade","annual_inc")])
loandata_ss$grade = as.factor(loandata_ss$grade)
N_ss = length(loandata_ss$grade)
#总体中各层的频率
PD_ss = table(loandata_ss$grade)/N_ss
#总共有7层
str = with(loandata_ss,levles(grade))
set.seed(20171101)
n = length(sampsize)
#定义分层抽样的函数
fun_ss <- function(s){
  p = NULL
  for (j in str){
    samp3 = NULL
    samp4 = sample((1:N_ss)[loandata_ss[,1]==j],round(s*PD_ss[j]))
    samp5 = loandata_ss[,2][samp4]
    p = c(p,samp5)
  }
  sampout = c(p,matrix(NA,1,sampsize[n]+10-length(p)))
  return(sampout)
}
sampsize  = as.matrix(sampsize)
#进行抽样
m_ss = data.frame(apply(sampsize,1,fun_ss))
#求样本质量
J = NULL
fun_ssq <- function(datasam1) {
  datasam11 <- cut(na.omit(datasam1),breaks = c(0,10000*(1:10),150000,100000*(2:5),max(loandata_ss$annual_inc)))
  PS <- table(datasam11)/length(na.omit(datasam1))+0.0000000001
  J <- sum((PS-PD_srs)*(log(PS/PD_srs)))
  q <- exp(-J)
  return(q)
}
Q2 = apply(m_ss,2,fun_ssq)

#对比简单随机抽样和分层随机抽样的样本质量
#以样本容量为横坐标，样本质量为纵坐标画散点图
par(mfrow=c(1,2))
plot(sampsize,Q1,xlab="样本容量",ylab="样本质量",
     main="简单随机抽样样本容量和样本质量的散点图")
plot(sampsize,Q2,xlab="样本容量",ylab="样本质量",
     main="分层随机抽样样本容量和样本质量的散点图")

#以样本容量为预测变量，样本质量为响应变量，拟合一条三次平滑样条曲线
#依据这条曲线，给定一个样本容量，我们可以知道估计出的样本质量
model_srs1 = smooth.spline(sampsize,Q1)
predict(model_srs1,sampsize)
model_ss1 = smooth.spline(sampsize,Q2)
predict(model_ss1,sampsize)

#也可以样本质量为预测变量，样本容量为响应变量，拟合一条三次平滑样条曲线
#依据这条曲线，给定一个目标样本质量，我们就可以得到一个估计的样本容量
model_srs2 = smooth.spline(Q1,sampsize)
model_ss2 = smooth.spline(Q2,sampsize)

#给定目标样本质量0.95和0.9999,预测样本容量
predict(model_srs2,0.9999)
predict(model_ss2,0.9999)
predict(model_srs2,0.95)
predict(model_ss2,0.95)

#-----------------------------------------------
#整群抽样
#-----------------------------------------------
loandata_cluster = na.omit(loandata[,c("annual_inc","zip_code")])
loandata_cluster$zip_code = as.factor(loandata_cluster$zip_code)
#按照zip_code分群，计算群的数量
clu = with(loandata_cluster,levels(zip_code))
length(clu)
N_clu = length(loandata_cluster$zip_code)
set.seed(20171101)
p = NULL
sampout1 = NULL
tempmatrix = matrix(c(seq(10,850,by = 10)))
#定义进行整群抽样的函数
fun_clu <- function(i){
    temp = sample(levels(loandata_cluster$zip_code),i)
    for(j in temp){
      samp1 = (1:N_clu)[loandata_cluster[,2]==j]
      p = c(p,samp1)
    }
    sampout1 = loandata_cluster[,1][p]
    sampout = c(sampout1,matrix(NA,1,235629-length(sampout1)))
    return(sampout)
}
#进行整群抽样
m_clu = data.frame(apply(tempmatrix,1,fun_clu))
#定义计算样本容量的函数
fun_calsampsize <- function(i){
  temp = sample(levels(loandata_cluster$zip_code),i)
  for(j in temp){
    samp1 = (1:N_clu)[loandata_cluster[,2]==j]
    p = c(p,samp1)
  }
  sampout = length(p)
  return(sampout)
}
#计算样本容量
ss = apply(tempmatrix,1,fun_calsampsize)
#定义求样本质量的函数
fun_cluster <- function(input){
  datasam = cut(input,
                breaks = c(0,10000*(1:10),150000,100000*(2:5),max(loandata_cluster$annual_inc)))
  PS = table(datasam)/length(na.omit(datasam))+0.0000000001
  J <- sum((PS-PD_srs)*(log(PS/PD_srs)))
  q <- exp(-J)
  return(q)
} 
#计算样本质量
Q3 = apply(m_clu,2,fun_cluster)
par(mfrow=c(1,2))
plot(ss,Q3,xlab="样本容量",ylab="样本质量")
plot(tempmatrix,Q3,xlab="群数",ylab="样本质量")
#拟合预测
model_clu = smooth.spline(Q3,ss)
predict(model_clu,0.9999)
predict(model_clu,0.95)
#---------------------------------------------------------------
#两阶段抽样
#---------------------------------------------------------------
loandata_twostages = na.omit(loandata[,c("annual_inc","addr_state")])
loandata_twostages$addr_state = as.factor(loandata_twostages$addr_state)
N_two = length(na.omit(loandata_twostages$annual_inc))
data_twostages = cut(loandata_twostages$annual_inc,
                   breaks = c(0,10000*(1:10),150000,100000*(2:5),max(loandata_twostages$annual_inc)))
PD_two = table(data_twostages)/N_two
#第一阶段抽样，从49个州中抽出10个州
#使用sampling包中的cluster函数
set.seed(20171101)
sampout_1_temp = loandata_twostages %>%
  cluster(clustername = "addr_state",size = 20,method="srswor")
sampout_1 = getdata(loandata_twostages,sampout_1_temp)$annual_inc
length(sampout_1)
#确定样本容量
#由于第一阶段抽样后留下80416条记录，因此控制样本容量小于这个值
x = seq(6,20,0.05)
y = 2^x
sampsize_two = round(y)[-c(207:281)]
sampsize_two
#定义第二阶段抽样的函数
n = length(sampsize_two)
p = NULL
fun_twostages <- function(input){
  set.seed(20171101)
  p = sample(sampout_1,input)
  p = c(p,matrix(NA,1,sampsize_two[n]-length(p)))
  return(p)
}
#第二阶段抽样
sampsize_two = as.matrix(sampsize_two)
sampoutput_twostages = data.frame(apply(sampsize_two,1,fun_twostages))
#计算样本质量
fun_twostagesq <- function(input){
  datasam = cut(input,
              breaks = c(0,10000*(1:10),150000,100000*(2:5),max(loandata_twostages$annual_inc)))
  PS = table(datasam)/length(na.omit(datasam))+0.0000000001
  J <- sum((PS-PD_two)*(log(PS/PD_two)))
  q <- exp(-J)
  return(q)
}
Q4 = apply(sampoutput_twostages,2,fun_twostages2)
plot(sampsize_two,Q4,xlab="样本容量",ylab="样本质量",main="两阶段抽样样本容量和样本质量的散点图")

#拟合预测
model_twostages = smooth.spline(Q4,sampsize_two)
predict(model_twostages,0.9999)
predict(model_twostages,0.95)
#完毕